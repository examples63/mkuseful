//
//  NetworkManager.swift
//  Daily Flights
//
//  Created by Martin Kompan on 13/12/2022.
//

import Foundation

public class NetworkManager {
    
    public static func fetchFromURL(_ path: String, completion: @escaping (Data?) -> Void) {
        guard let url = URL(string: path) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                completion(data)
            } else {
                NSLog("Network error: \(error?.localizedDescription ?? "Unknown")")
                completion(nil)
            }
        }
    }
    
    public static func getData(_ path: String, headers: [String:String]?,completion: @escaping (Data?) -> Void) {
        
        guard let url = URL(string: path) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        if let add = headers {
            add.forEach { request.addValue($0.value , forHTTPHeaderField: $0.key) }
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let data = data {
                completion(data)
            } else {
                NSLog("Network error: \(error?.localizedDescription ?? "Unknown")")
                completion(nil)
            }
        }
        task.resume()
    }
    
    public static func postData(_ path: String, requestBody: Encodable, completion: @escaping (Data?) -> Void) {
        guard let url = URL(string: path) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("json/application", forHTTPHeaderField: "Content-Type")
        do {
            let jsonReq = try JSONEncoder().encode(requestBody)
            request.httpBody = jsonReq
        } catch {
            NSLog("Request body parsing error for path: \(path)")
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let data = data {
                completion(data)
            } else {
                NSLog("Network error: \(error?.localizedDescription ?? "Unknown")")
                completion(nil)
            }
        }
    }
}
