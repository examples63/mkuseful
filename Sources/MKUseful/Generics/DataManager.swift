//
//  DataManager.swift
//  
//
//  Created by Martin Kompan on 15/12/2022.
//

import CoreData
import SwiftUI

open class DataManager {
    
    public static let shared = DataManager()
    
    @Environment(\.managedObjectContext) var moc
    
    public init(){}
    
    public func deleteAll<T: NSManagedObject>(_ entityType:T.Type){
        try? moc.deleteAll(T.self)
        try? moc.save()
    }
    
    public func saveContext() { try? moc.save() }
    
    public func getData<T: NSManagedObject>(_ entityType: T.Type, completion: @escaping ([T]?) -> Void) {
        do {
            let fetch = NSFetchRequest<T>(entityName: String(describing: T.self))
            let data = try moc.fetch(fetch)
            completion(data)
        } catch {
            completion(nil)
        }
    }
    
    public func isEmpty<T: NSManagedObject>(_ entityType: T.Type) -> Bool {
        do {
            let fetch = NSFetchRequest<T>(entityName: String(describing: T.self))
            let count = try moc.count(for: fetch)
            return count == 0
        } catch {
            return true
        }
    }
}
