//
//  CoreData-deleteAll.swift
//  Daily Flights
//
//  Created by Martin Kompan on 13/12/2022.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    
    func deleteAll(_ entity: NSManagedObject.Type) throws {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: NSStringFromClass(entity))
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try self.execute(deleteRequest)
        } catch let error as NSError {
            debugPrint(error)
            throw error
        }
    }
}
