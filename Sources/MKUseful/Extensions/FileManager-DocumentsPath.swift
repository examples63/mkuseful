//
//  FileManager-DocumentsPath.swift
//  Daily Flights
//
//  Created by Martin Kompan on 12/12/2022.
//

import Foundation

public extension FileManager {
    static var documentsDirectory: URL { FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] }
    
    static func loadData(_ path: String) -> Data? {
        let loadPath = FileManager.documentsDirectory.appendingPathComponent(path)
        do
        {
            return try Data(contentsOf: loadPath)
        } catch {
            NSLog("File Manager: Unable to load data from path: \(path)")
            return nil
        }
    }
    
    static func saveData(_ path: String, data: Data) {
        let savePath = FileManager.documentsDirectory.appendingPathComponent(path)
        do
        {
            try data.write(to: savePath, options: [.atomicWrite, .completeFileProtection])
        } catch {
            NSLog("File Manager: Unable to save data: \(data.description)")
        }
    }
}
