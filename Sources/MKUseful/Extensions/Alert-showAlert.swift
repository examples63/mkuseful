//
//  AlertExtension.swift
//  Daily Flights
//
//  Created by Martin Kompan on 12/12/2022.
//

import UIKit

public extension UIViewController {
    
    func showAlert(title: String, message: String) {
        let viewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        viewController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(viewController, animated: true, completion: nil)
    }
}
