//
//  Date-string.swift
//  Daily Flights
//
//  Created by Martin Kompan on 13/12/2022.
//

import Foundation

public extension Date {
    
    func stringWithFormat(_ dateFormat: String = "YYYY-MM-DDThh:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        
        return formatter.string(from: self)
    }
    
    static func todayWithFormat(_ dateFormat: String = "YYYY-MM-DDThh:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        
        return formatter.string(from: Date())
    }
}
