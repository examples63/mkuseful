
Martin Kompan ( martin.kompan@protonmail.com, kompan.martin@gmail.com )

# [CV and Portfolio 2012 - 2024](./CV_Martin_Kompan_Kotlin_Multiplatform.pdf)

# IQ: 127  (top 3,5% percentile)

## IQtest.com

<img src="./iq_1.png" width="600" />

<img src="./iq_2.png" width="550"/>

##  Free-iqtest.net

<img src="./iq_3.png" width="450" />

# Programming certificates

## LinkedIn assessments

### Swift (top 5%), Object-oriented programming (top 15%), Objective-C (top 30%)

<img src="./linkedin1.png" width="600" />

<img src="./linkedin2.png" width="550" />

<img src="./linkedin3.png" width="600" />

### TestDome (all top 10%, **Click to open:**)

[<img src="./Swift.png" width="500" />](https://www.testdome.com/certificates/72ef465ee4c648f89c49eadf909b05b6)
[<img src="./iOS_and_Swift.png" width="500" />](https://www.testdome.com/certificates/b222e6853ba9458ab535a46b2dff1d90)
[<img src="./Kotlin.png" width="500" />](https://www.testdome.com/certificates/b9726ca29e5f442daf40fb57c7dbfe15)
[<img src="./Android.png" width="500" />](https://www.testdome.com/certificates/8e114211bced4fdfb355514211133f98)
[!<img src="./Cybersecurity.png" width="500" />](https://www.testdome.com/certificates/a9cf5176dce7456f886f7741f6de8bc7)

