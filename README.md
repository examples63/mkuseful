# MKUseful

### Swift Package

[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Tested using **Xcode 14.1**

## Table of Contents

[My Programming Certificates List](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

### Extensions

[Alert + showAlert](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Extensions/Alert-showAlert.swift)

[CodeData (NSManagedObjectContext) + deleteAll](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Extensions/CoreData-deleteAll.swift)

[Date + stringWithFormat + todayWithFormat](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Extensions/Date-string.swift)

[FileManager + documentsDirectory + saveData + loadData](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Extensions/FileManager-DocumentsPath.swift)

### Generics

[CoreData manager](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Generics/DataManager.swift)

[Network Manager](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Generics/NetworkManager.swift)

### Code Snippets

**Add Code Snippets to Xcode:** copy them to "~/Library/Developer/Xcode/UserData/CodeSnippets" 

[CoreData init PersistenceController](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/CoreData_init_PersistenceController.codesnippet)

[Decodable (JSON parsing) + CoreData entity initialization](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/Decodable(JSON_parsing)%2BCoreData_entity_initialization.codesnippet)

[Network Manager + JSON parsing](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/Network_Manager%2BJSON_parsing.codesnippet)

[SwiftUI + Model (MVVM) + CoreData](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/SwiftUI%2BModel(MVVM)%2BCoreData.codesnippet)

[SwiftUI + ViewModel (MVVM) + CoreData](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/SwiftUI%2BViewModel(MVVM)%2BCoreData.codesnippet)

[SwiftUI + alert](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/SwiftUI%2Balert.codesnippet)

[SwiftUI View + navigation + MVVM + CoreData](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/SwiftUI_View%2Bnavigation%2BMVVM%2BCoreData.codesnippet)

[Static App Settings Constants Struct](https://gitlab.com/examples63/mkuseful/-/blob/main/Sources/MKUseful/Snippets/Struct_AppSettings.codesnippet)

## Release new version of MKUseful package:

Just add a version tag "X.Y.Z" [(Semantic Versioning)](https://semver.org/)

## Integration option 1: [Import as Swift package into Xcode](https://developer.apple.com/documentation/xcode/adding-package-dependencies-to-your-app)

(only manual updates, for auto-update use option 2 ) Open your Xcode Project (or Workspace), click on **File > Swift Packages > Add Package Dependency** and enter package git  [repository URL](https://gitlab.com/examples63/mkuseful.git) ( https://gitlab.com/examples63/mkuseful.git ) into **"Search or Enter Package URL"** input textfield.


## Integration option 2: Git subrepository

### Add subrepository into git
```
git submodule add https://gitlab.com/examples63/mkuseful.git
    
git add .
    
git commit -m "Add GitHub submodule"
```
### Add using Xcode's Swift Package Manager (auto-updates !)

Go to **Xcode** -> press **File** -> **Add packages...** -> **"Add local..."** button -> select the submodule's root git folder -> press **"Add package"** button

**Project's name** in navigation -> Project's **target** -> **General** -> find **"Frameworks, Libraries and Embedded Content"** list -> **"+"** button -> select **"MKUseful"** library -> **"Add"**


Now you should be able to use and build **MKUseful** package with your project.

**For auto-updating the package:**


**Project's name** in navigation -> Project's **target** -> **Build Phases** -> **New Run Script Phase** -> and add into it this:

```
git submodule foreach \

git pull origin main
```

